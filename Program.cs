﻿using System;
using System.IO;
using System.Collections.Generic;

namespace S10192609_MyBankApp
{
    class Program
    {
        static void Main(string[] args)
        {
            List<SavingsAccount> savingsAccList = new List<SavingsAccount> { };
            string[] accounts = File.ReadAllLines("savings_account.csv");
            for (int i = 1; i < accounts.Length; i++)
            {
                string[] temp = accounts[i].Split(",");
                savingsAccList.Add(new SavingsAccount(temp[0], temp[1], Convert.ToDouble(temp[2]), Convert.ToDouble(temp[3])));
            }
            string option;
            do
            {
                DisplayMenu();
                option = Console.ReadLine();
                Console.WriteLine();
                if (option == "1")
                {
                    DisplayAll(savingsAccList);
                }
                else if (option == "2")
                {
                    Console.Write("Enter the Account Number: ");
                    SavingsAccount account = Search(savingsAccList, Console.ReadLine());
                    if (account != null)
                    {
                        Console.Write("Amount to deposit: ");
                        double amount = Convert.ToDouble(Console.ReadLine());
                        Console.WriteLine("{0:$0.##} deposited successfully", amount);
                        account.Deposit(amount);
                        Console.WriteLine(account.ToString());
                    }
                    else
                    {
                        Console.WriteLine("Unable to find account number. Please try again.");
                    }
                    Console.WriteLine();
                }
                else if (option == "3")
                {
                    Console.Write("Enter the Account Number: ");
                    SavingsAccount account = Search(savingsAccList, Console.ReadLine());
                    if (account != null)
                    {
                        Console.Write("Amount to withdraw: ");
                        double amount = Convert.ToDouble(Console.ReadLine());
                        if (account.Withdraw(amount))
                        {
                            Console.WriteLine("{0:$0.##} withdrawn successfully", amount);
                            Console.WriteLine(account.ToString());
                        }
                        else
                        {
                            Console.WriteLine("Insufficient funds.");
                        }
                    }
                    else
                    {
                        Console.WriteLine("Unable to find account number. Please try again.");
                    }
                    Console.WriteLine();
                }
            } while (option != "0");
            Console.WriteLine("\n---------");
            Console.WriteLine("Goodbye!");
            Console.WriteLine("---------\n");
        }
        static void DisplayMenu()
        {
            Console.WriteLine("Menu");
            Console.WriteLine("[1] Display all accounts");
            Console.WriteLine("[2] Deposit");
            Console.WriteLine("[3] Withdraw");
            Console.WriteLine("[0] Exit");
            Console.Write("Enter option: ");
        }
        static void DisplayAll(List<SavingsAccount> sList)
        {
            foreach(SavingsAccount s in sList)
            {
                Console.WriteLine(s.ToString());
            }
        }
        static SavingsAccount Search(List<SavingsAccount> sList, string accNo)
        {
            foreach(SavingsAccount s in sList)
            {
                if (s.AccNo == accNo)
                {
                    return s;
                }
            }
            return null;
        }
    }
    class BankAccount
    {
        private string accNo;
        private string accName;
        private double balance;
        public string AccNo
        {
            get { return accNo; }
            set { accNo = value; }
        }
        public string AccName
        {
            get { return accName; }
            set { accName = value; }
        }
        public double Balance
        {
            get { return balance; }
            set { balance = value; }
        }

        public BankAccount() { }
        public BankAccount(string accNo, string accName, double balance)
        {
            AccNo = accNo;
            AccName = accName;
            Balance = balance;
        }
        public void Deposit(double dep)
        {
            Balance += dep;
        }
        public bool Withdraw(double amt)
        {
            if (amt <= balance)
            {
                Balance -= amt;
                return true;
            }
            else
            {
                return false;
            }
        }
        public override string ToString()
        {
            return "Acc No:" + accNo + " Acc Name:" + accName + " Balance:" + balance;
        }
    }
    class SavingsAccount: BankAccount
    {
        private double rate;
        public double Rate
        {
            get { return rate; }
            set { rate = value; }
        }
        public SavingsAccount() : base() { }
        public SavingsAccount(string accNo, string accName, double balance, double rate) : base(accNo, accName, balance)
        {
            Rate = rate;
        }
        public double CalculateInterest()
        {
            return Balance * rate / 100;
        }
        public override string ToString()
        {
            return base.ToString() + " Interest Rate:" + rate;
        }
    }
}
